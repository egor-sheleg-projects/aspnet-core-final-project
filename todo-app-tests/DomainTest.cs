﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using todo_domain_entities;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace todo_app_tests
{
    /// <summary>
    /// Tests for domain entities.
    /// </summary>
    [TestClass]
    public class DomainTest
    {
        /// <summary>
        /// Testing adding to database.
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            string arg2 = "Create db";
            DateTime arg3 = DateTime.Now;
            string arg4 = "Create db to EPAM";
            bool expected = true;

            // act
            bool result = Controller.AddTaskToList(arg1, arg2, arg3, arg4);

            // assert            
            Assert.AreEqual(expected, result);
            Controller.DeleteList(arg1);
        }

        /// <summary>
        /// Testing adding to database.
        /// </summary>
        [TestMethod]
        public void AddTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;
            string arg2 = "Create db";
            DateTime arg3 = DateTime.Now;
            string arg4 = "Create db to EPAM";

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.AddTaskToList(arg1, arg2, arg3, arg4));
        }

        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void DeleteListTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            bool expected = true;

            // act
            bool result = Controller.DeleteList(arg1);

            // assert            
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void DeleteListTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.DeleteList(arg1));
        }

        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void DeleteTaskTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            string arg2 = "TEST FOR UNIT TEST";
            bool expected = true;

            // act
            bool result = Controller.DeleteTaskFromList(arg1, arg2);

            // assert            
            Assert.AreEqual(expected, result);
            Controller.DeleteList(arg1);
        }

        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void DeleteTaskTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;
            string arg2 = "Create db";

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.DeleteTaskFromList(arg1, arg2));
        }

        /// <summary>
        /// Testing updating in database.
        /// </summary>
        [TestMethod]
        public void UpdateListTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            string arg2 = "UNIT TEST";
            bool expected = true;

            // act
            bool result = Controller.UpdateList(arg1, arg2);

            // assert            
            Assert.AreEqual(expected, result);
            Controller.DeleteList(arg2);
        }

        /// <summary>
        /// Testing updating in database.
        /// </summary>
        [TestMethod]
        public void UpdateListTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;
            string arg2 = "Create db";

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.UpdateList(arg1, arg2));
        }

        /// <summary>
        /// Testing completing in database.
        /// </summary>
        [TestMethod]
        public void CompleteListTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            bool expected = true;

            // act
            bool result = Controller.CompleteList(arg1);

            // assert            
            Assert.AreEqual(expected, result);
            Controller.DeleteList(arg1);
        }

        /// <summary>
        /// Testing completing in database.
        /// </summary>
        [TestMethod]
        public void CompleteListTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.CompleteList(arg1));
        }

        /// <summary>
        /// Testing completing in database.
        /// </summary>
        [TestMethod]
        public void CompleteTaskTest()
        {
            // arrange
            this.SetUp();
            string arg1 = "UNIT TEST";
            string arg2 = "UNIT TASK";
            bool expected = true;

            // act
            bool result = Controller.CompleteTask(arg1, arg2);

            // assert            
            Assert.AreEqual(expected, result);
            Controller.DeleteList(arg1);
        }

        /// <summary>
        /// Testing completing in database.
        /// </summary>
        [TestMethod]
        public void CompleteTaskTestThrowingNullExeption()
        {
            // arrange
            string arg1 = string.Empty;
            string arg2 = "Create db";

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.CompleteTask(arg1, arg2));
        }

        /// <summary>
        /// Creating a database element.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            Controller.CreateList("UNIT TEST", string.Empty, "Default");
            Controller.AddTaskToList("UNIT TEST", "UNIT TASK", DateTime.Now, "TEST FOR UNIT TEST");
        }
    }
}

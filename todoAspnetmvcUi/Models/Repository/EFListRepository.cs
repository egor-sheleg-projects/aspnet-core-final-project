﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using todo_domain_entities;

namespace todoAspnetmvcUi.Models.Repository
{
    public class EFListRepository : IListRepository
    {
        private TodoDbContext context;

        public EFListRepository(TodoDbContext ctx)
        {
            this.context = ctx;
        }

        public IQueryable<Todo_List> Todo_Lists => this.context.Todo_Lists
            .Include(o => o.Tasks);

        public IQueryable<Todo_Task> Todo_Task => this.context.Todo_Task;

        public void AddTask(Todo_List product, Todo_Task task)
        {
            Todo_List? dbEntry = this.context.Todo_Lists?.Include(o => o.Tasks).FirstOrDefault(p => p.Id == product.Id);

            if (dbEntry != null)
            {
                dbEntry.Name = product.Name;
                dbEntry.Description = product.Description;
                dbEntry.Category = product.Category;
                dbEntry.Tasks.Add(task);
            }

            this.context.SaveChanges();
        }

        public void Complete(Todo_Task product)
        {
            Todo_Task? dbEntry = this.context.Todo_Task?.FirstOrDefault(p => p.Id == product.Id);

            if (dbEntry != null)
            {
                dbEntry.IsComplete = !product.IsComplete;
            }

            this.context.SaveChanges();
        }

        public void CreateProduct(Todo_List product)
        {
            this.context.Add(product);
            this.context.SaveChanges();
        }

        public void DeleteProduct(Todo_List product)
        {
            this.context.Remove(product);
            this.context.SaveChanges();
        }

        public void DeleteProductTask(Todo_Task product)
        {
            this.context.Remove(product);
            this.context.SaveChanges();
        }

        public void SaveProduct(Todo_List product)
        {
            if (product.Id == 0)
            {
                this.context.Todo_Lists.Add(product);
            }
            else
            {
                Todo_List? dbEntry = this.context.Todo_Lists?.FirstOrDefault(p => p.Id == product.Id);

                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Category = product.Category;
                    dbEntry.Tasks = product.Tasks;
                }
            }

            this.context.SaveChanges();
        }

        public void SaveTask(Todo_Task product)
        {
            if (product.Id == 0)
            {
                this.context.Todo_Task.Add(product);
            }
            else
            {
                Todo_Task? dbEntry = this.context.Todo_Task?.FirstOrDefault(p => p.Id == product.Id);

                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.DateStart = product.DateStart;
                    dbEntry.Date = product.Date;
                    dbEntry.IsComplete = product.IsComplete;
                    dbEntry.Tasks = product.Tasks;
                }
            }

            this.context.SaveChanges();
        }
    }
}

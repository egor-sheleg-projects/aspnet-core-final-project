﻿using System.Linq;
using todo_domain_entities;

namespace todoAspnetmvcUi.Models.Repository
{
    public interface IListRepository
    {
        IQueryable<Todo_List> Todo_Lists { get; }

        IQueryable<Todo_Task> Todo_Task { get; }

        void Complete(Todo_Task task);

        void AddTask(Todo_List product, Todo_Task task);

        void SaveProduct(Todo_List product);

        void SaveTask(Todo_Task product);

        void CreateProduct(Todo_List product);

        void DeleteProduct(Todo_List product);

        void DeleteProductTask(Todo_Task product);
    }
}

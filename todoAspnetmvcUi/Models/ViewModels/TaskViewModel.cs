﻿using todo_domain_entities;

namespace todoAspnetmvcUi.Models.ViewModels
{
    public class TaskViewModel
    {
        public Todo_Task Task { get; set; } = new Todo_Task();

        public string ReturnUrl { get; set; } = "/";
    }
}

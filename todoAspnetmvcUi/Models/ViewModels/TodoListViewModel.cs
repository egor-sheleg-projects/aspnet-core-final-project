﻿using System.Collections.Generic;
using System.Linq;
using todo_domain_entities;

namespace todoAspnetmvcUi.Models.ViewModels
{
    public class TodoListViewModel
    {
        public IEnumerable<Todo_List> Todo_Lists { get; set; } = Enumerable.Empty<Todo_List>();

        public PagingInfo PagingInfo { get; set; }

        public string CurrentCategory { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities;

namespace todoAspnetmvcUi.Models
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext(DbContextOptions<TodoDbContext> options)
        : base(options)
        {
        }

        public DbSet<Todo_List> Todo_Lists => this.Set<Todo_List>();

        public DbSet<Todo_Task> Todo_Task => this.Set<Todo_Task>();
    }
}

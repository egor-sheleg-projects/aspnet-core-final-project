﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;
using todoAspnetmvcUi.Models.Repository;

namespace todoAspnetmvcUi.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IListRepository repository;

        public NavigationMenuViewComponent(IListRepository repository)
        {
            this.repository = repository;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(repository.Todo_Lists
               .Select(x => x.Category)
               .Distinct());
        }
    }

}

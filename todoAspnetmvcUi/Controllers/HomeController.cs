﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using todoAspnetmvcUi.Models.Repository;
using todoAspnetmvcUi.Models.ViewModels;
using todo_domain_entities;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using Microsoft.EntityFrameworkCore;

namespace todoAspnetmvcUi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IListRepository repository;
        private int pageSize = 4;

        public HomeController(IListRepository repository)
        {
            this.repository = repository;
        }

        [Route("Details/{productId:int}")]
        public ViewResult Details(int productId)
            => this.View(this.repository.Todo_Lists.Include(o => o.Tasks).FirstOrDefault(p => p.Id == productId));

        public Todo_Task Task { get; set; }

        [Route("Edit/{productId:long}")]
        public ViewResult Edit(int productId)
        {
            return View(this.repository.Todo_Lists.FirstOrDefault(p => p.Id == productId));
        }

        [HttpGet, Route("Today")]
        public IActionResult Today(string? category, int productPage = 1)
                      => this.View(new TodoListViewModel
                      {
                          Todo_Lists = this.repository.Todo_Lists
                          .Where(p => category == null || p.Category == category)
                          .OrderBy(p => p.Id)
                          .Skip((productPage - 1) * this.pageSize)
                          .Take(this.pageSize),
                          PagingInfo = new PagingInfo
                          {
                              CurrentPage = productPage,
                              ItemsPerPage = this.pageSize,
                              TotalItems = category == null ? this.repository.Todo_Lists.Count() : this.repository.Todo_Lists.Where(e => e.Category == category).Count(),
                          },

                          CurrentCategory = category,
                      });

        [HttpPost]
        [Route("Edit/{productId:long}")]
        public IActionResult Edit(Todo_List product)
        {
            if (this.ModelState.IsValid)
            {
                this.repository.SaveProduct(product);
                return this.RedirectToAction("Index");
            }

            return this.View(product);
        }

        [Route("EditTask/{productId:long}")]
        public ViewResult EditTask(int productId)
            => this.View(this.repository.Todo_Task.FirstOrDefault(p => p.Id == productId));

        [HttpPost]
        [Route("EditTask/{productId:long}")]
        public IActionResult EditTask(Todo_Task product)
        {
            this.repository.SaveTask(product);
            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [Route("Complete/{productId:long}")]
        public IActionResult CompleteProduct(int productId)
        {
            var product = this.repository.Todo_Task.FirstOrDefault(p => p.Id == productId);
            this.repository.Complete(product);
            return this.RedirectToAction("Index");
        }

        [Route("Complete/{productId:long}")]
        public ViewResult Complete(int productId)
            => View(this.repository.Todo_Task?.FirstOrDefault(p => p.Id == productId));


        [Route("AddTask/{productId:long}")]
        public ViewResult AddTask(int productId)
        {
            return View(this.repository.Todo_Lists?.Include(o => o.Tasks).FirstOrDefault(p => p.Id == productId));
        }

        [HttpPost]
        [Route("AddTask/{productId:long}")]
        public IActionResult AddTask(Todo_List product, Todo_Task task)
        {
            if (this.ModelState.IsValid)
            {
                this.repository.AddTask(product, task);
                return this.RedirectToAction("Index");
            }

            return View(product);
        }

        [Route("Create")]
        public ViewResult Create()
        {
            return this.View(new Todo_List());
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(Todo_List product)
        {
            if (this.ModelState.IsValid)
            {
                this.repository.SaveProduct(product);
                return this.RedirectToAction("Index");
            }

            return this.View(product);
        }

        [Route("DeleteTask/{productId:long}")]
        public IActionResult DeleteTask(int productId)
            => this.View(this.repository.Todo_Task.FirstOrDefault(p => p.Id == productId));

        [HttpPost]
        [Route("DeleteTask/{productId:long}")]
        public IActionResult DeleteProductTask(int productId)
        {
            var product = this.repository.Todo_Task.FirstOrDefault(p => p.Id == productId);
#pragma warning disable CS8604 // Possible null reference argument.
            this.repository.DeleteProductTask(product);
#pragma warning restore CS8604 // Possible null reference argument.
            return this.RedirectToAction("Index");
        }
        [Route("Delete/{productId:long}")]
        public IActionResult Delete(int productId)
            => this.PartialView(this.repository.Todo_Lists.FirstOrDefault(p => p.Id == productId));

        [HttpPost]
        [Route("Delete/{productId:long}")]
        public IActionResult DeleteProduct(int productId)
        {
            var product = this.repository.Todo_Lists.FirstOrDefault(p => p.Id == productId);
#pragma warning disable CS8604 // Possible null reference argument.
            this.repository.DeleteProduct(product);
#pragma warning restore CS8604 // Possible null reference argument.
            return this.RedirectToAction("Index");
        }

        [Route("Index")]
        public ViewResult Products() => this.View(this.repository.Todo_Lists);

        public ViewResult Index(string? category, int productPage = 1)
                      => this.View(new TodoListViewModel
                      {
                          Todo_Lists = this.repository.Todo_Lists
                          .Where(p => category == null || p.Category == category)
                          .OrderBy(p => p.Id)
                          .Skip((productPage - 1) * this.pageSize)
                          .Take(this.pageSize),
                          PagingInfo = new PagingInfo
                          {
                              CurrentPage = productPage,
                              ItemsPerPage = this.pageSize,
                              TotalItems = category == null ? this.repository.Todo_Lists.Count() : this.repository.Todo_Lists.Where(e => e.Category == category).Count(),
                          },

                          CurrentCategory = category,
                      });
    }
}

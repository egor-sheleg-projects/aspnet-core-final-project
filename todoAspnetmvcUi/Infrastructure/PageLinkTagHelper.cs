﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using todoAspnetmvcUi.Models.ViewModels;

namespace todoAspnetmvcUi.TagHelpers
{
    [HtmlTargetElement("div", Attributes = "page-model")]
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory urlHelperFactory;

        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        public PagingInfo PageModel { get; set; }

        public string PageAction { get; set; }

        public bool PageClassesEnabled { get; set; }

        public string PageClass { get; set; } = string.Empty;

        public string PageClassNormal { get; set; } = string.Empty;

        public string PageClassSelected { get; set; } = string.Empty;

        public string PageRoute { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (ViewContext != null && PageModel != null)
            {
                var urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);
                var result = new TagBuilder("div");
                for (var i = 1; i <= PageModel.TotalPages; i++)
                {
                    var tag = new TagBuilder("a");
                    PageUrlValues[key: "productPage"] = i;
                    tag.Attributes[key: "href"] = urlHelper.Action(action: PageAction, values: PageUrlValues);
                    tag.Attributes[key: "href"] = urlHelper.RouteUrl(routeName: PageRoute, values: PageUrlValues);

                    tag.Attributes["href"] = urlHelper.Action(
                        PageAction,
                        new { productPage = i });
                    tag.InnerHtml.Append(i.ToString(CultureInfo.CurrentCulture));
                    result.InnerHtml.AppendHtml(tag);
                    if (PageClassesEnabled)
                    {
                        tag.AddCssClass(PageClass);
                        tag.AddCssClass(i == PageModel.CurrentPage
                            ? PageClassSelected : PageClassNormal);
                    }
                }

                output.Content.AppendHtml(result.InnerHtml);
            }
        }
    }

}

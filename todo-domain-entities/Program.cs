﻿using System;

namespace todo_domain_entities
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string button = string.Empty;
            while (button != "q")
            {
                Console.WriteLine("Enter -c to create new TODO list, -a to add task in exist TODO list, -dl to delete TODO list, -dt to delete task in TODO list, -v to get TODO lists, -ut to update task in list, -ul to update TODO list, -ct to complete task, -cl to complete tasks in list.");
                Console.WriteLine("Enter q to quit.");
                button = Console.ReadLine();
                bool result = false;
                switch (button)
                {
                    case "-c":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            Console.WriteLine("Enter description of list:");
                            string descList = Console.ReadLine();
                            Console.WriteLine("Enter category of list:");
                            string categoryList = Console.ReadLine();
                            result = Controller.CreateList(nameList, descList, categoryList);
                            break;
                        }
                    case "-v":
                        {
                            Controller.GetLists();
                            break;
                        }
                    case "-a":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            string reader = string.Empty;
                            while (reader != "q")
                            {
                                Console.WriteLine("Enter task title (or q to finish creating tasks):");
                                reader = Console.ReadLine();
                                if (reader != "q")
                                {
                                    Console.WriteLine("Enter task description:");
                                    string task = Console.ReadLine();
                                    Console.WriteLine("Enter date to end of the task:");
                                    DateTime userDateTime;
                                    if (DateTime.TryParse(Console.ReadLine(), out userDateTime))
                                    {
                                        result = Controller.AddTaskToList(nameList, reader, userDateTime, task);
                                    }
                                    else
                                    {
                                        Console.WriteLine("You have entered an incorrect value. Enter task again.");
                                    }
                                }
                            }
                            break;
                        }
                    case "-ut":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            result = Controller.UpdateTaskInList(nameList);
                            Console.WriteLine(result);
                            break;
                        }
                    case "-ul":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            Console.WriteLine("Enter new list name:");
                            string newnameList = Console.ReadLine();
                            result = Controller.UpdateList(nameList, newnameList);
                            Console.WriteLine(result);
                            break;
                        }
                    case "-dt":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            Console.WriteLine("Enter task name:");
                            string taskname = Console.ReadLine();
                            result = Controller.DeleteTaskFromList(nameList, taskname);
                            Console.WriteLine(result);
                            break;
                        }
                    case "-dl":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            result = Controller.DeleteList(nameList);
                            break;
                        }
                    case "-ct":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            Console.WriteLine("Enter task name which is complete:");
                            string taskname = Console.ReadLine();
                            result = Controller.CompleteTask(nameList, taskname);
                            break;
                        }
                    case "-cl":
                        {
                            Console.WriteLine("Enter list name:");
                            string nameList = Console.ReadLine();
                            result = Controller.CompleteList(nameList);
                            break;
                        }
                }

                if (result is true)
                {
                    Console.WriteLine("Operation is done.");
                }
            }
        }
    }
}

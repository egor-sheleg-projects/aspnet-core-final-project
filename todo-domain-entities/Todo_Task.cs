﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public class Todo_Task
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime Date { get; set; }

        public bool IsComplete { get; set; }

        public string Tasks { get; set; }
    }
}

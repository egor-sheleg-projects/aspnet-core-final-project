﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace todo_domain_entities
{
    public static class Controller
    {
        public static bool CreateList(string nameList, string descList, string categoryList)
        {
            if (string.IsNullOrEmpty(categoryList))
            {
                categoryList = "Default";
            }
            using (ApplicationContext db = new ApplicationContext())
            {
                var list = new Todo_List { Name = nameList, Description = descList, Category = categoryList };

                db.Todo_Lists.AddRange(list);
                db.SaveChanges();
            }

            return true;
        }

        public static bool AddTaskToList(string nameList, string nameTask, DateTime dateTime, string entertask)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {
                    List<Todo_Task> tasks = new List<Todo_Task>();

                    Todo_Task task = new Todo_Task();
                    task.Name = nameTask;
                    task.Tasks = entertask;
                    task.DateStart = DateTime.Today;
                    task.Date = dateTime;
                    task.IsComplete = false;
                    tasks.Add(task);

                    lists.Tasks.AddRange(tasks);
                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }

            }

            return true;
        }

        public static bool DeleteTaskFromList(string nameList, string taskname)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {
                    List<Todo_Task> tasks = lists.Tasks;

                    Todo_Task task = tasks.Find(x => x.Name == taskname);
                    tasks.Remove(task);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool DeleteList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();
                if (lists != null)
                {
                    db.Remove(lists);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool UpdateTaskInList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {
                    string button = string.Empty;

                    List<Todo_Task> tasks = lists.Tasks;

                    Console.WriteLine("Enter task name to edit:");
                    string taskname = Console.ReadLine();
                    Todo_Task task = tasks.Find(x => x.Name == taskname);
                    Console.WriteLine("Enter task new name:");
                    button = Console.ReadLine();
                    task.Name = button;
                    Console.WriteLine("Enter new task:");
                    button = Console.ReadLine();
                    task.Tasks = button;
                    int i = 0;

                    do
                    {
                        Console.WriteLine("Enter date to end of the task:");
                        DateTime userDateTime;
                        if (DateTime.TryParse(Console.ReadLine(), out userDateTime))
                        {
                            task.Date = userDateTime;
                            task.IsComplete = false;
                            i = 1;
                        }
                        else
                        {
                            Console.WriteLine("You have entered an incorrect value.");
                        }
                    }
                    while (i == 0);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool UpdateList(string nameList, string newnameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {

                    lists.Name = newnameList;

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool CompleteTask(string nameList, string taskname)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {
                    List<Todo_Task> tasks = lists.Tasks;

                    Todo_Task task = tasks.Find(x => x.Name == taskname);
                    task.IsComplete = true;

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool CompleteList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<Todo_List>();

                if (lists != null)
                {
                    List<Todo_Task> tasks = lists.Tasks;

                    foreach (Todo_Task t in tasks)
                    {
                        t.IsComplete = true;
                    }

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static void GetLists()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.Todo_Lists.Include(x => x.Tasks).ToList();
                foreach (Todo_List i in lists)
                {
                    Console.WriteLine($"---{i.Name}---");
                    Console.WriteLine($"{i.Description}");
                    Console.WriteLine($"{i.Category}");
                    if (i.Tasks != null)
                    {
                        foreach (var s in i.Tasks)
                        {
                            Console.WriteLine($"{s.Name} --- {s.Tasks}| Date:{s.Date}| Creation date: {s.DateStart} | Is completed:{s.IsComplete}");
                        }
                    }
                }
            }
        }
    }
}

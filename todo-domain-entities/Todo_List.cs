﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public class Todo_List
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }

        public string Description { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please enter a category")]
        public string Category { get; set; } = "Default";

        public List<Todo_Task> Tasks { get; set; } = new List<Todo_Task> { };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace todo_domain_entities
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Todo_List> Todo_Lists { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=todoapplication;Trusted_Connection=True;");
        }
    }
}
